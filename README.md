* Docker.io is being utilized as the builder of this application. 
* [Docker Documentation](https://docs.docker.com/get-started/part2/#dockerfile)
* Three files are needed for this application (Dockerfile, app.py, requirements.txt).

# 1. Create Dockerfile

    FROM python:2.7-slim
    WORKDIR /app
    ADD . /app
    RUN pip install -r requirements.txt
    EXPOSE 80
    ENV NAME World
    CMD ["python", "app.py"]

# 2. Create requirements.txt 

     Flask 
     Redis

# 3. Create app.py

	from flask import Flask
	from redis import Redis, RedisError
	import os
	import socket

	# Connect to Redis
	redis = Redis(host="redis", db=0, socket_connect_timeout=2, socket_timeout=2)

	app = Flask(__name__)

	@app.route("/")
	def hello():
	    try:
	        visits = redis.incr('counter')
	    except RedisError:
	        visits = "<i>cannot connect to Redis, counter disabled</i>"

	    html = "<h3>Hello {name}!</h3>" \
	           "<b>Hostname:</b> {hostname}<br/>" \
	           "<b>Visits:</b> {visits}"
	    return html.format(name=os.getenv('NAME', "world"), hostname=socket.gethostname(), visits=visits)

	if __name__ == "__main__":
		app.run(host='0.0.0.0', port=80)


# 4 
If stored locally, then
1) CD into the docker directory
2) Build the image and tag it 

   docker build -t friendlyhello .
   
3) Login
 
    docker login

4) Prepare for Upload 

    docker tag friendlyhello chriscruz/dockertest:firsttag
	

5) Push image to Docker

     docker push chriscruz/dockertest:firsttag
	 
6) Have Docker on any machine

      docker run -p 80:80 chriscruz/dockertest:firsttag

check local-host to make sure running correctly - http://localhost/
